// pages/change/change.js
const app = getApp()
const db = wx.cloud.database()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    content: '',
    QQ: '',
    relation: '',
    id: ''
  },
  //修改数据
  formSubmit: function (e) {
    var that=this;
    db.collection('address').doc(that.data.id).update({
      data: {
        "name": e.detail.value.name,
        "content": e.detail.value.content,
        "QQ": e.detail.value.QQ,
        "relation": e.detail.value.relation
      },
      success: function (res) {
        console.log(res.data)
        wx.showToast({
          title: '修改成功',
        })
        setTimeout(function(){
          wx.navigateBack({
            url: '../page/page',
          })
        }, 1500)
      }
    })
  },
  onLoad: function (option) {
    var that=this;
    db.collection('address').doc(option.id).get({
      success: function (res) {
        console.log(res.data)
        that.setData({ 
          name: res.data.name,
           content: res.data.content, 
          QQ: res.data.QQ, 
          relation: res.data.relation, 
          id: res.data._id
          })
      }
    })
  },

})