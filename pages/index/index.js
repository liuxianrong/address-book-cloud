// pages/index/index.js
const app = getApp()
const db = wx.cloud.database()
Page({
  /**
   * 页面的初始数据
   */
  data: {
    searchContent: ''
  },
  //搜索
  formSubmit: function (e) {
    var that = this;
    // db.collection('address').where({
    //   name: e.detail.value.searchContent,
    // })
    //   .get({
    //     success: function (res) {
    //       console.log(res.data)
    //       if(res.data.length>0){
    //       that.setData({ posts_key: res.data })
    //       }else{
    //         wx.showModal({
    //           title: '提示',
    //           content: '查无此人',
    //         })
    //       }
    //     }
    //   })
    db.collection('address').where({
      name: db.RegExp({
        regexp: e.detail.value.searchContent,
        //从搜索栏中获取的value作为规则进行匹配。
        options: 'i',
        //大小写不区分
      })
    })
      .get({
        success: function (res) {
          console.log(res.data)
          if (res.data.length > 0) {
          that.setData({ posts_key: res.data })
          }else{
            wx.showModal({
              title: '提示',
              content: '查无此人',
            })
          }
        }    
    }) 
  },
  //跳转至修改页面
  change: function (e) {
    console.log(e.currentTarget.id)
    wx.navigateTo({
      url: '../change/change?id=' + e.currentTarget.id,
    })
  },
  //跳转至添加页面
  add: function () {
    wx.navigateTo({
      url: '../add/add',
    })
  },
  //删除一条内容
  delNote: function (event) {
    var that=this;
    //console.log(event.currentTarget.id)
    wx.showModal({
      title: '提示',
      content: '是否确定删除',
      success(res) {
        if (res.confirm) {
          db.collection('address').doc(event.currentTarget.id).remove({
            success: function (res) {
              console.log(res)
              wx.showToast({
                title: '删除成功',
              })
              setTimeout(function(){
              that.onShow();
              },1000)
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
    
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
     var that=this;
    // db.collection('address').get({
    //   success: function (res) {
    //     // res.data 是一个包含集合中有权限访问的所有记录的数据，不超过 20 条
    //     console.log(res.data)
    //     that.setData({ 
    //       posts_key: res.data 
    //       })
    //   }
    // })
    wx.cloud.callFunction({
      name:'getAlladdress',
      success: function (res){
        console.log(res.result.data)
        that.setData({
          posts_key: res.result.data
          })
      },
      fail: console.error
    })
  },
  onPullDownRefresh: function () {
    var that=this;
    wx.showNavigationBarLoading() //在标题栏中显示加载

    //模拟加载

    setTimeout(function () {

      // complete
      that.onShow();

      wx.hideNavigationBarLoading() //完成停止加载

      wx.stopPullDownRefresh() //停止下拉刷新

    }, 1500);
  },
})