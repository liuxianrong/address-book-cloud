//index.js
//获取应用实例
const app = getApp()
const db = wx.cloud.database()

Page({
  /**
   * 页面的初始数据
   */
  data: {
    name: '',
    content: '',
    QQ: '',
    relation: ''
  },
  formSubmit: function (e) {
    db.collection('address').add({
      // data 字段表示需新增的 JSON 数据
      data: {
        "name": e.detail.value.name,
        "content": e.detail.value.content,
        "QQ": e.detail.value.QQ,
        "relation": e.detail.value.relation
      }
    })
      .then(res => {
        console.log(res)
        if (res.errMsg =="collection.add:ok"){
          wx.showToast({
            title: '添加成功',
          })
          setTimeout(function () {
            wx.navigateBack({
              url: '../page/page',
            })
          }, 1500)
        }else{
          wx.showModal({
            title: '提示',
            content: '保存失败',
          })
        }
      })
  }
})

